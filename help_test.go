package goconfigure_test

import (
	"fmt"

	"bitbucket.org/idomdavis/goconfigure"
)

func ExampleHelpOption() {
	opts := goconfigure.Options{}
	opts.Add(goconfigure.HelpOption())

	_ = opts.ParseUsing([]string{"-h"})

	fmt.Println(goconfigure.Help)

	// Output: true
}
