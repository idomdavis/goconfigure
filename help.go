package goconfigure

// Help will be set to true if the HelpOption has been registered and the
// relevant flag set on the command line. If Help is true then the application
// should output usage information and exit.
var Help bool

// HelpOption returns a flag configured on `h` and `help` which will be set to
// goconfigure.Help when parsed.
func HelpOption() *Option {
	return &Option{
		ShortFlag:   'h',
		LongFlag:    "help",
		Description: "Display this help",
		Pointer:     &Help,
		Default:     false,
	}
}
