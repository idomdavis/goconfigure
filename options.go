package goconfigure

import (
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"bitbucket.org/idomdavis/goconfigure/value"
)

// OptionSet that can be added to.
type OptionSet interface {
	Add(option *Option)
	Flag(i interface{}, name rune, description string) *Option
	Option(i, d interface{}, name, description string) *Option
}

// Options holds a set of configuration options which can be provided by the
// command line, environment variables, configuration files, or default values.
type Options struct {
	// Stub is an optional stub that can be prepended to environment variable
	// names in the format Stub_Name.
	Stub string

	external *External
	uri      *Option
	data     []*Option
}

// ErrFailedToReadURI is returned if the URI for the config file is not in the
// expected format.
var ErrFailedToReadURI = errors.New("failed to read URI for config file")

// Match all non word characters.
var strip = regexp.MustCompile(`\W`)

// Add an Option to this set of Options.
func (o *Options) Add(option *Option) {
	o.data = append(o.data, option)
}

// Option returns an option with i being the Option.Pointer to set (which must
// be one of *bool, *int, *int64, *uint, *uint64, *float64, *string, or
// *time.Duration) and d being the default value of the same type, or nil for no
// default. The description is used when producing usage information. Providing
// an invalid type for i or d will not error here, but will generate an error
// when Option.Check is called. The Option.LongFlag and Option.ConfigKey are
// both set to name. Option.EnvVar is set to name with all `-` characters
// changed to `_`, and the entire string in upper case.
func (o *Options) Option(i, d interface{}, name, description string) *Option {
	return &Option{
		LongFlag:    name,
		EnvVar:      o.ToEnv(name),
		ConfigKey:   name,
		Description: description,
		Pointer:     i,
		Default:     d,
	}
}

// Flag returns an option that only uses a command line flag. i is the
// Option.Pointer to set (which must be one of *bool, *int, *int64, *uint,
// *uint64, *float64, *string, or *time.Duration) The description is used when
// producing usage information. Providing an invalid type for i will not error
// here, but will generate an error when Option.Check is called.
func (o *Options) Flag(i interface{}, name rune, description string) *Option {
	return &Option{
		ShortFlag:   name,
		Description: description,
		Pointer:     i,
	}
}

// LoadUsing configuration file, the URI for which will be obtained from the
// given option. The option must resolve to a string. By definition this option
// can't be read from the configuration file and must come from a flag or
// environment variable.
func (o *Options) LoadUsing(option *Option) {
	o.uri = option
}

// Parse the Options.
func (o *Options) Parse() error {
	return o.ParseUsing(os.Args[1:])
}

// ParseUsing uses the given arguments as the set of command line arguments.
//nolint:gocyclo,cyclop,funlen
func (o *Options) ParseUsing(args []string) error {
	flags := NewFlags()
	env := NewEnv()

	env.Stub = o.Stub

	if o.external == nil {
		o.external = &External{}
	}

	for _, opt := range o.data {
		if err := opt.Check(); err != nil {
			return fmt.Errorf("option validation error: %w", err)
		}

		if err := flags.Register(opt); err != nil {
			return fmt.Errorf("flag error: %w", err)
		}

		if err := env.Register(opt); err != nil {
			return fmt.Errorf("env error: %w", err)
		}
	}

	if err := flags.Parse(args); err != nil {
		return fmt.Errorf("error parsing flags: %w", err)
	}

	if o.uri != nil {
		if err := o.uri.Set(
			flags.Data[string(o.uri.ShortFlag)],
			flags.Data[o.uri.LongFlag],
			value.Data{},
			value.Data{},
		); err != nil {
			return fmt.Errorf("error getting config file URI: %w", err)
		}

		if uri, ok := o.uri.Pointer.(*string); !ok {
			return fmt.Errorf("%w: %T not a string",
				ErrFailedToReadURI, o.uri.Pointer)
		} else if *uri == "" {
			// do nothing
		} else if err := o.external.Load(*uri); err != nil {
			return fmt.Errorf("error loading config file: %w", err)
		}
	}

	for i, opt := range o.data {
		external, err := o.external.Value(opt)

		if err != nil {
			return fmt.Errorf("config file error: %w", err)
		}

		if err := o.data[i].Set(
			flags.Data[string(opt.ShortFlag)],
			flags.Data[opt.LongFlag],
			env.Data[opt.EnvVar],
			external,
		); err != nil {
			return fmt.Errorf("config error: %w", err)
		}
	}

	return nil
}

// Usage displays the usage information for this set of options to the given
// writer. An error writing the usage will cause a panic. A nil writer will do
// nothing.
func (o *Options) Usage(w io.Writer) {
	if w == nil {
		return
	}

	b := strings.Builder{}
	b.WriteString("Usage: ")
	b.WriteString(filepath.Base(os.Args[0]))
	b.WriteString(" <options>:\n\nOptions:\n")
	b.WriteString(o.UsageString())

	if _, err := fmt.Fprintln(w, b.String()); err != nil {
		panic(err)
	}
}

// UsageString building of custom usage output by providing just the usage
// details for the defined options.
func (o *Options) UsageString() string {
	b := strings.Builder{}

	for _, opt := range o.data {
		b.WriteString(opt.Usage())
	}

	if len(o.data) == 0 {
		b.WriteString("        No configuration options set")
	}

	b.WriteString("\n")

	return b.String()
}

// ToEnv will turn a name into an environment variable by replacing all non word
// characters with '_'.
func (o *Options) ToEnv(name string) string {
	env := strip.ReplaceAllString(name, "_")

	if o.Stub == "" {
		return strings.ToUpper(env)
	}

	return strings.ToUpper(o.Stub + "_" + env)
}
