package goconfigure

import (
	"fmt"
	"sort"
	"strings"

	"github.com/sirupsen/logrus"
)

// A Reporter is used to report configuration information.
type Reporter interface {

	// Report the configuration block. The description is an arbitrary text
	// string used to identify the block, the data is the set of options. It is
	// save to assume the data will be a primitive, or fmt.Stringer.
	Report(description string, data map[string]interface{})
}

// ConsoleReporter reports information to STDOUT.
type ConsoleReporter struct{}

// Report to STDOUT.
func (c ConsoleReporter) Report(description string, data map[string]interface{}) {
	var i int

	if len(data) == 0 {
		fmt.Println(description)
		return
	}

	keys := make([]string, len(data))
	entries := make([]string, len(data))

	for k := range data {
		keys[i] = k
		i++
	}

	sort.Strings(keys)

	for i, k := range keys {
		entries[i] = fmt.Sprintf("%s:%v", k, data[k])
	}

	fmt.Printf("%s: [%s]\n", description, strings.Join(entries, ", "))
}

// LogrusReporter reports via Logrus.
type LogrusReporter struct {
	Logger *logrus.Logger
}

// Report to Logrus.
func (l LogrusReporter) Report(description string, data map[string]interface{}) {
	if l.Logger == nil {
		l.Logger = logrus.StandardLogger()
	}

	if len(data) == 0 {
		l.Logger.Info(description)
	} else {
		l.Logger.WithFields(data).Info(description)
	}
}

// NullReporter will not report anything.
type NullReporter struct{}

// Report nothing.
func (NullReporter) Report(string, map[string]interface{}) {}
