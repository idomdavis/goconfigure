package goconfigure_test

import (
	"errors"
	"fmt"
	"os"
	"testing"

	"bitbucket.org/idomdavis/goconfigure"
)

func Example_no_config_file() {
	// Use Settings from goconfigure_test
	settings := &Settings{}

	s := goconfigure.NewSettings("TEST")
	s.AddConfigFile()
	s.Add(settings)

	reporter := goconfigure.ConsoleReporter{}

	if err := s.ParseUsing([]string{"-m", "msg", "--count", "2"}, reporter); err != nil {
		fmt.Println(err)
	}

	// Output:
	// Example Options Block: [Display Message:msg, Message Count:2, Unset Option:This can't get set]
}

func TestSettings_Parse(t *testing.T) {
	t.Run("Parse uses OS arguments", func(t *testing.T) {
		var option string

		s := goconfigure.NewSettings("test")

		s.Options.Add(&goconfigure.Option{Pointer: &option, ShortFlag: 'o'})

		var args []string
		copy(args, os.Args)
		defer func() { copy(os.Args, args) }()
		os.Args = []string{"app", "-o", "set"}

		if err := s.Parse(nil); err != nil {
			t.Errorf("unexpected error parsing options: %v", err)
		}

		if option != "set" {
			t.Errorf("unexpect option value: %q", option)
		}
	})
}

func TestSettings_ParseUsing(t *testing.T) {
	t.Run("ParseUsing will error if configuration fails", func(t *testing.T) {
		s := goconfigure.NewSettings("test")

		err := s.ParseUsing([]string{"-fail"}, nil)

		if !errors.Is(err, goconfigure.ErrFlagError) {
			t.Errorf("unexpected error parsing settings: %v", err)
		}
	})

	t.Run("ParseUsing will accept env vars", func(t *testing.T) {
		var option string

		_ = os.Setenv("TEST_VALUE", "set")
		defer func() { _ = os.Setenv("TEST_VALUE", "") }()

		s := goconfigure.NewSettings("TEST")
		s.Options.Add(s.Options.Option(&option, "", "value", ""))

		err := s.ParseUsing([]string{}, goconfigure.NullReporter{})

		switch {
		case err != nil:
			t.Errorf("Unexpected error: %v", err)
		case option != "set":
			t.Errorf("Unexpected option value %q", option)
		}
	})
}

func TestSettings_Parse_lazy_init(t *testing.T) {
	t.Run("Parse will lazy initialise options", func(t *testing.T) {
		s := goconfigure.Settings{}
		s.Add(&validBlock{})

		if err := s.ParseUsing([]string{}, nil); err != nil {
			t.Errorf("unexpected error parsing options: %v", err)
		}
	})
}

type mockBlock struct{}

func (mockBlock) Register(_ goconfigure.OptionSet) {}
func (mockBlock) Description() string              { return "mock" }

type validBlock struct{ mockBlock }

func (validBlock) Data() interface{} {
	return struct{ Value int }{Value: 1}
}

type invalidTypeBlock struct{ mockBlock }

func (invalidTypeBlock) Data() interface{} {
	return struct{ Invalid chan int }{make(chan int)}
}

type invalidDataBlock struct{ mockBlock }

func (invalidDataBlock) Data() interface{} {
	return []int{}
}

func TestSettings_Add(t *testing.T) {
	t.Run("A block cannot use invalid data types", func(t *testing.T) {
		s := goconfigure.NewSettings("test")
		s.Add(invalidTypeBlock{})

		err := s.ParseUsing([]string{""}, nil)

		if !errors.Is(err, goconfigure.ErrInvalidDataType) {
			t.Errorf("enexpected error with invalid block: %v", err)
		}
	})

	t.Run("A block cannot use invalid structs", func(t *testing.T) {
		s := goconfigure.NewSettings("test")
		s.Add(invalidDataBlock{})

		err := s.ParseUsing([]string{""}, nil)

		if !errors.Is(err, goconfigure.ErrInvalidDataFormat) {
			t.Errorf("enexpected error with invalid block: %v", err)
		}
	})
}
