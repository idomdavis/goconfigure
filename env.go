package goconfigure

import (
	"fmt"
	"os"

	"bitbucket.org/idomdavis/goconfigure/value"
)

// Env holds the settings passed from environment variables.
type Env struct {
	Stub string
	Data map[string]value.Data
}

// NewEnv returns a new, empty set of environment variable settings.
func NewEnv() *Env {
	return &Env{Data: map[string]value.Data{}}
}

// Register an option, getting the environment variable for the option if it's
// set. If a Stub is set on the Env this is prepended to the EnvVar name with
// a _ in the form Stub_o.EnvVar.
func (e *Env) Register(o *Option) error {
	if env := os.Getenv(o.EnvVar); env != "" {
		v, err := value.Coerce(env, o.Pointer)

		if err != nil {
			return fmt.Errorf("failed to parse environment option '%s': %w",
				o.EnvVar, err)
		}

		e.Data[o.EnvVar] = v
	}

	return nil
}
