package goconfigure_test

import (
	"os"

	"bitbucket.org/idomdavis/goconfigure"
	"github.com/sirupsen/logrus"
)

func ExampleLogrusReporter_Report() {
	var l goconfigure.Reporter

	logger := logrus.New()
	logger.SetOutput(os.Stdout)
	logger.SetFormatter(&logrus.TextFormatter{
		DisableColors:    true,
		DisableTimestamp: true,
	})

	l = goconfigure.LogrusReporter{}
	l.Report("data", map[string]interface{}{"k": "v"})
	l.Report("message", nil)

	l = goconfigure.LogrusReporter{Logger: logger}
	l.Report("data", map[string]interface{}{"k": "v"})
	l.Report("message", nil)

	l = goconfigure.ConsoleReporter{}
	l.Report("data", map[string]interface{}{"k": "v"})
	l.Report("message", nil)

	l = goconfigure.NullReporter{}
	l.Report("data", map[string]interface{}{"k": "v"})
	l.Report("message", nil)

	// Output:
	// level=info msg=data k=v
	// level=info msg=message
	// data: [k:v]
	// message
}
