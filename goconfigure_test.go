package goconfigure_test

import (
	"fmt"
	"os"

	"bitbucket.org/idomdavis/goconfigure"
)

type Settings struct {
	Count   int    `json:"Message Count"`
	Message string `json:"Display Message"`
	Unset   string `json:"Unset Option"`
}

func (s *Settings) Description() string {
	return "Example Options Block"
}

// Data can override the values in the backing type to hide or enhance what is
// reported about the settings.
func (s *Settings) Data() interface{} {
	return Settings{
		Count:   s.Count,
		Message: s.Message,
		// Here we will display the contents of s.Unset if it is set, otherwise
		// a generic unset message will be shown.
		Unset: goconfigure.Sanitise(s.Unset, s.Unset, goconfigure.UNSET),
	}
}

func (s *Settings) Register(opts goconfigure.OptionSet) {
	opts.Add(&goconfigure.Option{
		Pointer:     &s.Message,
		Description: "The message to display",
		ShortFlag:   'm',
		ConfigKey:   "message",
		Default:     "This space intentionally left blank",
	})

	opts.Add(opts.Option(&s.Count, 1, "count",
		"The number of times to display the message"))

	opts.Add(&goconfigure.Option{
		Pointer: &s.Unset,
		Default: "This can't get set",
	})
}

func Example() {
	settings := &Settings{}

	// When using environment variables they will all be of the form
	//    EXAMPLE_<name>
	s := goconfigure.NewSettings("EXAMPLE")
	s.AddHelp()
	s.AddConfigFile()
	s.Add(settings)

	args := []string{"-c", "testdata/options.json", "--count", "2"}
	reporter := goconfigure.ConsoleReporter{}

	// You can use opts.Parse() to grab the command line arguments, however,
	// passing in the arguments makes testing easier.
	if err := s.ParseUsing(args, reporter); err != nil {
		fmt.Println(err)
		s.Options.Usage(os.Stdout)
		os.Exit(-1)
	}

	for i := 0; i < settings.Count; i++ {
		fmt.Println(settings.Message)
	}

	fmt.Println(settings.Unset)
	fmt.Println(s.UsageString())

	// Output:
	// Example Options Block: [Display Message:Hello, world!, Message Count:2, Unset Option:This can't get set]
	// Hello, world!
	// Hello, world!
	// This can't get set
	//
	//   -h, --help
	//         Display this help
	//   -c, --config
	//         Configure via the given configuration file.
	//         Use $EXAMPLE_CONFIG_FILE to set this using environment variables.
	//   -m
	//         The message to display (default "This space intentionally left blank")
	//         Use 'message' to set this in the config file.
	//   --count
	//         The number of times to display the message (default 1)
	//         Use $EXAMPLE_COUNT to set this using environment variables.
	//         Use 'count' to set this in the config file.
	//   No CLI option
	//          (default "This can't get set")
}
