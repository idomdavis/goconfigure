package goconfigure

import "errors"

// Block of Options. The block can Register itself against a set of Options and
// return its Description and current Data. The block Data should map to a
// map[string]interface{}.
type Block interface {

	// Description of the option block. Description is used when reporting on
	// the options set.
	Description() string

	// Register the options in the block against the given OptionSet.
	Register(opts OptionSet)

	// Data set by the options. The type returned by Data should map to a
	// map[string]interface{} and is used when reporting on the options set
	// with the data being displayed as key/value pairs. All data to be
	// displayed must be on public properties and the display key can be set
	// using the json tag.
	Data() interface{}
}

// ErrInvalidDataType is returned by Settings.Parse and Settings.ParseUsing if
// the Block.Data function returns a struct with data types that are not
// supported by goconfigure.
var ErrInvalidDataType = errors.New("invalid data type")

// ErrInvalidDataFormat is returned by Settings.Parse and Settings.ParseUsing if
// the Block.Data function returns a struct that will not fit in a
//     map[string]interface{}
var ErrInvalidDataFormat = errors.New("invalid data format")
