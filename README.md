# Configuration Package for Go

[![Build](https://img.shields.io/bitbucket/pipelines/idomdavis/goconfigure?style=plastic)](https://bitbucket.org/idomdavis/goconfigure/addon/pipelines/home)
[![Issues](https://img.shields.io/bitbucket/issues-raw/idomdavis/goconfigure?style=plastic)](https://bitbucket.org/idomdavis/goconfigure/issues)
[![Pull Requests](https://img.shields.io/bitbucket/pr-raw/idomdavis/goconfigure?style=plastic)](https://bitbucket.org/idomdavis/goconfigure/pull-requests/)
[![Go Doc](http://img.shields.io/badge/godoc-reference-5272B4.svg?style=plastic)](http://godoc.org/github.com/idomdavis/goconfigure)
[![License](https://img.shields.io/badge/license-MIT-green?style=plastic)](https://opensource.org/licenses/MIT)

`goconfigure` allows configuration of an application via flags, environment
variables, or a configuration file. 

## Installation

```
go get bitbucket.org/idomdavis/goconfigure
```


## Usage

The following is derived from `goconfigure_test.go` which can be used to see how 
the various methods of setting options works:

```go
package main

import (
	"fmt"
	"os"

	"bitbucket.org/idomdavis/goconfigure"
)

type Settings struct {
	Count   int    `json:"Message Count"`
	Message string `json:"Display Message"`
	Unset   string `json:"Unset Option"`
}

func (s *Settings) Description() string {
	return "Example Options Block"
}

func (s *Settings) Data() interface{} {
	return s
}

func (s *Settings) Register(opts goconfigure.OptionSet) {
	opts.Add(&goconfigure.Option{
		Pointer:     &s.Message,
		Description: "The message to display",
		ShortFlag:   'm',
		ConfigKey:   "message",
		Default:     "This space intentionally left blank",
	})

	opts.Add(goconfigure.NewOption(&s.Count, 1, "count",
		"The number of times to display the message"))

	opts.Add(&goconfigure.Option{
		Pointer: &s.Unset,
		Default: "This can't get set",
	})
}

func main() {
	settings := &Settings{}

	s := goconfigure.Settings{}
	s.AddHelp()
	s.AddConfigFile()
	s.Add(settings)

	args := []string{"-c", "testdata/options.json"}
	reporter := goconfigure.ConsoleReporter{}

	// Ordinarily we'd just use opts.Parse() to grab the command line arguments.
	if err := s.ParseUsing(args, reporter); err != nil {
		fmt.Println(err)
		s.Options.Usage()
		os.Exit(-1)
	}

	for i := 0; i < settings.Count; i++ {
		fmt.Println(settings.Message)
	}

	fmt.Println(settings.Unset)

	// Output:
	// Example Options Block: [Display Message:Hello, world!, Message Count:3, Unset Option:This can't get set]
	// Hello, world!
	// Hello, world!
	// Hello, world!
	// This can't get set
}
```

If this were in a file called `example.go` then you would get the following:

```
$ go run ./example.go 
This space intentionally left blank
```

Using an options file gives:

```
$ cat options.json
{
  "message": "Hello, world!",
  "count": 3
}
$ go run ./example.go -o ./options.json
Hello, world!
Hello, world!
Hello, world!
```

Options can be overridden, with command line arguments always taking 
precedence:

```
$ GOCONFIGURE_COUNT=2 go run example.go -o ./options.json 
Hello, world!
Hello, world!
```

```
$ GOCONFIGURE_COUNT=2 go run example.go -o ./options.json --count 4
Hello, world!
Hello, world!
Hello, world!
Hello, world!
```
