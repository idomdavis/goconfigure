package goconfigure_test

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"bitbucket.org/idomdavis/goconfigure"
)

func TestExternal_Load(t *testing.T) {
	t.Run("An error calling the target will be reported", func(t *testing.T) {
		e := goconfigure.External{}
		err := e.Load("https://notfound")

		if !errors.Is(err, goconfigure.ErrLoadingConfig) {
			t.Errorf("unexpected error loading external config: %v", err)
		}
	})

	t.Run("An invalid url will fail", func(t *testing.T) {
		e := goconfigure.External{}
		err := e.Load("https://notfound\t")

		if !errors.Is(err, goconfigure.ErrLoadingConfig) {
			t.Errorf("unexpected error loading external config: %v", err)
		}
	})

	t.Run("An invalid body will fail", func(t *testing.T) {
		e := goconfigure.External{}
		server := httptest.NewServer(http.HandlerFunc(
			func(w http.ResponseWriter, r *http.Request) {
				w.Header().Set("Content-Length", "1")
			}))

		err := e.Load(server.URL)

		if !errors.Is(err, goconfigure.ErrLoadingConfig) {
			t.Errorf("unexpected error parsing options: %v", err)
		}
	})

	t.Run("Invalid JSON will fail", func(t *testing.T) {
		e := goconfigure.External{}

		server := httptest.NewServer(http.HandlerFunc(
			func(w http.ResponseWriter, r *http.Request) {
				_, _ = w.Write([]byte(`{"key": `))
			}))

		err := e.Load(server.URL)

		if !errors.Is(err, goconfigure.ErrParsingConfig) {
			t.Errorf("unexpected error parsing options: %v", err)
		}
	})
}

func TestExternal_Register(t *testing.T) {
	t.Run("Durations are correctly handled", func(t *testing.T) {
		var p time.Duration

		e := goconfigure.External{}

		server := httptest.NewServer(http.HandlerFunc(
			func(w http.ResponseWriter, r *http.Request) {
				_, _ = w.Write([]byte(`{"key": "1h"}`))
			}))

		_ = e.Load(server.URL)

		if v, err := e.Value(&goconfigure.Option{
			ConfigKey: "key",
			Pointer:   &p,
		}); err != nil {
			t.Fatalf("unexpected error parsing duration: %v", err)
		} else if err = v.AssignTo(&p); err != nil {
			t.Fatalf("unexpected error assigning duration: %v", err)
		} else if p.String() != "1h0m0s" {
			t.Errorf("Unexpected duration: %s", p.String())
		}
	})

	t.Run("Invalid durations are correctly handled", func(t *testing.T) {
		var p time.Duration

		e := goconfigure.External{}

		server := httptest.NewServer(http.HandlerFunc(
			func(w http.ResponseWriter, r *http.Request) {
				_, _ = w.Write([]byte(`{"key": "d"}`))
			}))

		_ = e.Load(server.URL)

		_, err := e.Value(&goconfigure.Option{
			ConfigKey: "key",
			Pointer:   &p,
		})

		if !errors.Is(err, goconfigure.ErrInvalidDuration) {
			t.Errorf("unexpected error parsing duration: %v", err)
		}
	})

	t.Run("A type mismatch will fail", func(t *testing.T) {
		var p int

		e := goconfigure.External{}

		server := httptest.NewServer(http.HandlerFunc(
			func(w http.ResponseWriter, r *http.Request) {
				_, _ = w.Write([]byte(`{"key": "value"}`))
			}))

		_ = e.Load(server.URL)

		_, err := e.Value(&goconfigure.Option{
			ConfigKey: "key",
			Pointer:   &p,
		})

		if !errors.Is(err, goconfigure.ErrConversionFailure) {
			t.Errorf("unexpected error parsing options: %v", err)
		}
	})
}
