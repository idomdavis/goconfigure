package goconfigure

import (
	"errors"
	"flag"
	"fmt"
	"os"
	"time"

	"bitbucket.org/idomdavis/goconfigure/value"
)

// Flags holds the settings passed in from the command line.
type Flags struct {
	Data map[string]value.Data

	flagSet *flag.FlagSet
}

var (
	// ErrFlagError is returned when there is an error parsing the flags. It is
	// used to wrap errors from the flags package which are not wrapped errors.
	ErrFlagError = errors.New("flag parse error")

	// ErrInvalidTypeOption is returned when the type on the option cannot be
	// set via a flag.
	ErrInvalidTypeOption = errors.New("invalid option type for flag")

	// ErrInvalidDefault is returned if the default value cannot be used on the
	// type set for the option.
	ErrInvalidDefault = errors.New("cannot use default option")
)

// NewFlags returns a new, empty set of flags.
func NewFlags() *Flags {
	flagSet := flag.NewFlagSet(os.Args[0], flag.ContinueOnError)
	flagSet.Usage = func() {}

	return &Flags{
		Data:    map[string]value.Data{},
		flagSet: flagSet,
	}
}

// Parse the flags using the given arguments.
func (f *Flags) Parse(args []string) error {
	if err := f.flagSet.Parse(args); err != nil {
		msg := err.Error()
		return fmt.Errorf("%w caused by: %s", ErrFlagError, msg)
	}

	f.flagSet.Visit(func(current *flag.Flag) {
		if d, ok := f.Data[current.Name]; ok {
			d.Set = true
			f.Data[current.Name] = d
		}
	})

	return nil
}

// Register an option. If they are set the long and short value for that option
// are set once Flags.Parse is called.
func (f *Flags) Register(o *Option) error {
	if o.ShortFlag != 0 {
		name := string(o.ShortFlag)

		if err := f.register(name, o); err != nil {
			return fmt.Errorf("failed to set short flag %s: %w", name, err)
		}
	}

	if o.LongFlag != "" {
		name := o.LongFlag

		if err := f.register(name, o); err != nil {
			return fmt.Errorf("failed to set long flag %s: %w", name, err)
		}
	}

	return nil
}

//nolint:gocyclo,cyclop
func (f *Flags) register(name string, o *Option) error {
	var (
		p  interface{}
		ok bool
	)

	switch o.Pointer.(type) {
	case *bool:
		v, success := o.Default.(bool)

		p = f.flagSet.Bool(name, v, o.Description)
		ok = success
	case *int:
		v, success := o.Default.(int)

		p = f.flagSet.Int(name, v, o.Description)
		ok = success
	case *int64:
		v, success := o.Default.(int64)
		p = f.flagSet.Int64(name, v, o.Description)
		ok = success
	case *uint:
		v, success := o.Default.(uint)

		p = f.flagSet.Uint(name, v, o.Description)
		ok = success
	case *uint64:
		v, success := o.Default.(uint64)

		p = f.flagSet.Uint64(name, v, o.Description)
		ok = success
	case *float64:
		v, success := o.Default.(float64)

		p = f.flagSet.Float64(name, v, o.Description)
		ok = success
	case *string:
		v, success := o.Default.(string)

		p = f.flagSet.String(name, v, o.Description)
		ok = success
	case *time.Duration:
		v, success := o.Default.(time.Duration)

		p = f.flagSet.Duration(name, v, o.Description)
		ok = success
	default:
		return fmt.Errorf("%w %q: %T", ErrInvalidTypeOption, name, o.Pointer)
	}

	if !ok && o.Default != nil {
		return fmt.Errorf("%w %v (%[2]T) as %T for flag %q",
			ErrInvalidDefault, o.Default, o.Pointer, name)
	}

	f.Data[name] = value.Data{Pointer: p}

	return nil
}
