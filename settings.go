package goconfigure

import (
	"encoding/json"
	"fmt"
	"os"
)

// Settings all for configuration of an app via Options and sets of Block types.
type Settings struct {
	Help   bool
	Blocks []Block

	*Options

	config string
}

// NewSettings returns an empty Settings type set to use the given stub for
// environment variables.
func NewSettings(stub string) *Settings {
	return &Settings{Options: &Options{Stub: stub}}
}

// Add a Block to the Settings. Settings may have any number of blocks, but
// generally have at least 1.
func (s *Settings) Add(block Block) {
	s.Blocks = append(s.Blocks, block)
}

// AddHelp to the Settings. This will register `-h` and `--help. Settings.Help
// will be set to true if the flag is passed.
func (s *Settings) AddHelp() {
	s.lazyInit()

	s.Options.Add(&Option{
		ShortFlag:   'h',
		LongFlag:    "help",
		Description: "Display this help",
		Pointer:     &s.Help,
		Default:     false,
	})
}

// AddConfigFile to the Settings. This will register `-c` and `--config`, plus
// check <stub>_CONFIG_FILE and load settings from the file if these are set.
func (s *Settings) AddConfigFile() {
	s.lazyInit()

	opt := &Option{
		ShortFlag:   'c',
		LongFlag:    "config",
		EnvVar:      s.ToEnv("CONFIG_FILE"),
		Description: "Configure via the given configuration file.",
		Pointer:     &s.config,
	}

	s.Options.Add(opt)
	s.Options.LoadUsing(opt)
}

// Parse the Settings using the Reporter to output the results.
func (s *Settings) Parse(reporter Reporter) error {
	return s.ParseUsing(os.Args[1:], reporter)
}

// ParseUsing allows Settings to be run using custom arguments rather than
// os.Args.
func (s *Settings) ParseUsing(args []string, reporter Reporter) error {
	s.lazyInit()

	for _, b := range s.Blocks {
		b.Register(s.Options)
	}

	if err := s.Options.ParseUsing(args); err != nil {
		return fmt.Errorf("failed to configure application: %w", err)
	}

	for _, b := range s.Blocks {
		var values map[string]interface{}

		data := b.Data()
		description := b.Description()

		if bytes, err := json.Marshal(data); err != nil {
			return fmt.Errorf("%w %T: %s", ErrInvalidDataType, data, err.Error())
		} else if err = json.Unmarshal(bytes, &values); err != nil {
			return fmt.Errorf("%w %T : %s", ErrInvalidDataFormat, data, err.Error())
		}

		if reporter != nil && description != "" {
			reporter.Report(description, values)
		}
	}

	return nil
}

func (s *Settings) lazyInit() {
	if s.Options == nil {
		s.Options = &Options{}
	}
}
