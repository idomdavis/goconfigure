package value_test

import (
	"errors"
	"fmt"
	"reflect"
	"strings"
	"testing"
	"time"

	"bitbucket.org/idomdavis/goconfigure/value"
)

func ExampleCoerce() {
	var typeOf string
	if v, err := value.Coerce("text", &typeOf); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}

	// Output:
	// {true text}
}

func ExampleData_AssignTo() {
	var v string
	d := value.New("example")
	if err := d.AssignTo(&v); err != nil {
		fmt.Println(err)
	}

	fmt.Println(v)

	// Output:
	// example
}

func TestCoerce(t *testing.T) {
	const prefix = "value.Data: cannot coerce 'invalid':"

	for _, test := range []struct {
		Type     interface{}
		Input    string
		Expected interface{}
		Skip     bool
	}{
		{Type: true, Input: "t", Expected: true},
		{Type: 0, Input: "1", Expected: 1},
		{Type: int64(0), Input: "1", Expected: int64(1)},
		{Type: uint(0), Input: "1", Expected: uint(1)},
		{Type: uint64(0), Input: "1", Expected: uint64(1)},
		{Type: float64(0), Input: "1", Expected: float64(1)},
		{Type: "", Input: "text", Expected: "text", Skip: true},
		{Type: time.Duration(0), Input: "1ns", Expected: time.Nanosecond},
	} {
		t.Run(fmt.Sprintf("coerce %T", test.Type), func(t *testing.T) {
			if v, err := value.Coerce(test.Input, &test.Type); err != nil {
				t.Errorf("Unexpected error coercing %T: %s",
					test.Type, err.Error())
			} else if v.Pointer != test.Expected {
				t.Errorf("Expected %v, got %v coercing %T",
					test.Expected, v.Pointer, test.Type)
			}
		})
		t.Run(fmt.Sprintf("coerce invalid %T", test.Type), func(t *testing.T) {
			// Some coercions never fail
			if test.Skip {
				return
			}

			_, err := value.Coerce("invalid", test.Type)
			if err == nil || !strings.HasPrefix(err.Error(), prefix) {
				t.Errorf("Unexpected error coercing invalid %T: %v",
					test.Type, err)
			}
		})
	}

	t.Run("invalid type", func(t *testing.T) {
		var p []string
		_, err := value.Coerce("invalid", &p)

		if !errors.Is(err, value.ErrInvalidType) {
			t.Errorf("Unexpected error coercing invalid value: %v", err)
		}
	})
}

func TestData_AssignTo(t *testing.T) {
	for _, test := range []struct {
		Receiver interface{}
		Value    interface{}
		Error    error
	}{
		{Value: 1},
		{Receiver: new(bool), Value: true},
		{Receiver: new(int), Value: 1},
		{Receiver: new(int64), Value: int64(1)},
		{Receiver: new(uint), Value: uint(1)},
		{Receiver: new(uint64), Value: uint64(1)},
		{Receiver: new(float64), Value: float64(1)},
		{Receiver: new(string), Value: "text"},
		{Receiver: new(string), Value: new(string)},
		{Receiver: new(time.Duration), Value: time.Duration(1)},
		{Receiver: "", Value: "text", Error: value.ErrInvalidPointerType},
		{Receiver: new(string), Value: 1, Error: value.ErrAssignmentFailure},
		{Receiver: new([]string), Value: true, Error: value.ErrAssignmentFailure},
		{Receiver: new([]string), Value: []string{"text"},
			Error: value.ErrInvalidPointerType},
	} {
		name := fmt.Sprintf("assign %T to %T", test.Value, test.Receiver)
		t.Run(name, func(t *testing.T) {
			v := value.New(test.Value)
			err := v.AssignTo(test.Receiver)

			switch {
			case !errors.Is(err, test.Error):
				t.Errorf("Expected %v, got %v", test.Error, err)
			case err != nil:
			case test.Value != v.Pointer:
				t.Errorf("Expected %v, got %v", test.Value, v.Pointer)
			case test.Receiver == nil || reflect.ValueOf(test.Value).IsZero():
			case reflect.ValueOf(test.Receiver).IsZero():
				t.Errorf("%T receiver not set", test.Receiver)
			}
		})
	}
}
