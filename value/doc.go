// Package value handles mapping values to the settings type. The package can
// handle input from a number of source and allows values to be mapped to a
// provided pointer so that the calling program can deal with concrete types.
package value
