package value

import (
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"time"
)

// Data holds an untyped (interface{}) value which can be assigned to a bool,
// int, int64, uint, uint64, float64, string, or time.Duration.
type Data struct {
	Set     bool
	Pointer interface{}
}

var (
	// ErrInvalidType is returned if an attempt is made to Coerce data to an
	// unsupported type.
	ErrInvalidType = errors.New("value.Data: invalid type")

	// ErrAssignmentFailure is returned if the Data type can't be assigned to
	// the given type.
	ErrAssignmentFailure = errors.New("value.Data: assignment failure")

	// ErrInvalidPointerType is returned if the Pointer in the Data type isn't
	// supported.
	ErrInvalidPointerType = errors.New("value.Data: invalid pointer type")
)

// New creates a new Data type with the given value.
func New(data interface{}) Data {
	return Data{Set: true, Pointer: data}
}

// Coerce the given string into a Data type holding a value of typeOf. If the
// data cannot be coerced the the underlying parse error will be returned. An
// invalid typeOf will result in ErrInvalidType.
//nolint:cyclop
func Coerce(data string, typeOf interface{}) (Data, error) {
	var (
		r   interface{}
		err error
	)

	t := Chase(typeOf)

	switch {
	case t == reflect.TypeOf(time.Duration(1)):
		r, err = time.ParseDuration(data)
	case t.Kind() == reflect.Bool:
		r, err = strconv.ParseBool(data)
	case t.Kind() == reflect.Int:
		v, e := strconv.ParseInt(data, 10, 64)
		r, err = int(v), e
	case t.Kind() == reflect.Int64:
		r, err = strconv.ParseInt(data, 10, 64)
	case t.Kind() == reflect.Uint:
		v, e := strconv.ParseUint(data, 10, 64)
		r, err = uint(v), e
	case t.Kind() == reflect.Uint64:
		r, err = strconv.ParseUint(data, 10, 64)
	case t.Kind() == reflect.Float64:
		r, err = strconv.ParseFloat(data, 64)
	case t.Kind() == reflect.String:
		r = data
	default:
		err = fmt.Errorf("%w: got %T, chased to %v", ErrInvalidType, typeOf, t)
	}

	if err != nil {
		err = fmt.Errorf("value.Data: cannot coerce '%s': %w", data, err)
	}

	return New(r), err
}

// AssignTo sets the given pointer to point to the value held by this Data type.
// ErrInvalidPointerType is returned if the given pointer is not a valid type,
// or is not a reflect.Ptr. ErrAssignmentFailure is returned if there is an
// error assigning the Data type to the pointer.
//nolint:gocyclo,cyclop
func (d Data) AssignTo(pointer interface{}) (err error) {
	var to, from reflect.Value

	defer func() {
		if r := recover(); r != nil {
			err = fmt.Errorf("%w %v '%v' to %v", ErrAssignmentFailure,
				from.Type(), from.Interface(), to.Type())
		}
	}()

	if pointer == nil || d.Pointer == nil {
		return nil
	}

	from = reflect.ValueOf(d.Pointer)
	to = reflect.ValueOf(pointer)

	if from.Kind() == reflect.Ptr {
		from = reflect.Indirect(from)
	}

	if to.Kind() == reflect.Ptr {
		to = reflect.Indirect(to)
	} else {
		return fmt.Errorf("%w: %v should be *%[2]v",
			ErrInvalidPointerType, to.Type())
	}

	data := from.Convert(to.Type())

	switch p := pointer.(type) {
	case *bool:
		*p = data.Bool()
	case *int:
		*p = int(data.Int())
	case *int64:
		*p = data.Int()
	case *uint:
		*p = uint(data.Uint())
	case *uint64:
		*p = data.Uint()
	case *float64:
		*p = data.Float()
	case *string:
		if from.Kind() != reflect.String {
			return fmt.Errorf("%w: cannot convert %v to string",
				ErrAssignmentFailure, from.Kind())
		}

		*p = data.String()
	case *time.Duration:
		*p = time.Duration(data.Int())
	default:
		return fmt.Errorf("%w: %T", ErrInvalidPointerType, p)
	}

	return nil
}

// Chase down the actual type of an interface. If it's a pointer get the
// type being pointed to, if that's an interface get the underlying type for
// that.
func Chase(in interface{}) reflect.Type {
	underlying := reflect.ValueOf(in)

	if underlying.Kind() == reflect.Ptr {
		underlying = underlying.Elem()
	}

	if underlying.Kind() == reflect.Interface {
		underlying = underlying.Elem()
	}

	return underlying.Type()
}
