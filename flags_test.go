package goconfigure_test

import (
	"errors"
	"testing"

	"bitbucket.org/idomdavis/goconfigure"
)

func TestFlags_Parse(t *testing.T) {
	t.Run("Failure to parse the flag set will error", func(t *testing.T) {
		f := goconfigure.NewFlags()

		_ = f.Register(&goconfigure.Option{LongFlag: "name"})

		err := f.Parse([]string{"-undefined"})

		if !errors.Is(err, goconfigure.ErrFlagError) {
			t.Errorf("Unexpected error parsing flags: %v", err)
		}
	})
}

func TestFlags_Register(t *testing.T) {
	t.Run("Short flags with mismatched types will error", func(t *testing.T) {
		var (
			p int
			d string
		)

		f := goconfigure.NewFlags()

		err := f.Register(&goconfigure.Option{
			ShortFlag: 'f',
			Pointer:   &p,
			Default:   &d,
		})

		if !errors.Is(err, goconfigure.ErrInvalidDefault) {
			t.Errorf("Unexpected error registering flags: %v", err)
		}
	})

	t.Run("Long flags with mismatched types will error", func(t *testing.T) {
		var (
			p int
			d string
		)

		f := goconfigure.NewFlags()

		err := f.Register(&goconfigure.Option{
			LongFlag: "flag",
			Pointer:  &p,
			Default:  &d,
		})

		if !errors.Is(err, goconfigure.ErrInvalidDefault) {
			t.Errorf("Unexpected error registering flags: %v", err)
		}
	})
}
