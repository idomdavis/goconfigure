package goconfigure

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"reflect"
	"strings"
	"time"

	"bitbucket.org/idomdavis/goconfigure/value"
)

// External configuration data. This can either be loaded as a JSON file from a
// URI (path or URL), or be set explicitly.
type External map[string]interface{}

var (
	// ErrLoadingConfig is returned if a config file cannot be read from the
	// path or URL given.
	ErrLoadingConfig = errors.New("error loading config")

	// ErrParsingConfig is returned if the config file is not valid JSON.
	ErrParsingConfig = errors.New("error parsing config")

	// ErrInvalidDuration is returned if the config file contains an invalid
	// duration string.
	ErrInvalidDuration = errors.New("invalid duration value")

	// ErrConversionFailure is returned if a value from a config file cannot be
	// applied to the Option for that value.
	ErrConversionFailure = errors.New("cannot convert config type")
)

// Load external configuration from a URI. The configuration details for an
// Option can be retrieved using Value.
func (e *External) Load(uri string) error {
	var f = ioutil.ReadFile

	if strings.HasPrefix(uri, "http://") || strings.HasPrefix(uri, "https://") {
		f = get
	}

	if b, err := f(uri); err != nil {
		return fmt.Errorf("%w from %q: %v", ErrLoadingConfig, uri, err)
	} else if err = json.Unmarshal(b, e); err != nil {
		m := err.Error()

		return fmt.Errorf("%w %q: %s", ErrParsingConfig, uri, m)
	}

	return nil
}

// Value for the given Option. If the Option has not been set in the
// external config, or the external config has not been loaded, then an empty,
// unset value.Data is returned. Returns ErrConversionFailure if the value set
// in config cannot be applied to the Option. Returns ErrInvalidDuration if an
// invalid duration string is given.
func (e *External) Value(o *Option) (value.Data, error) {
	data := *e
	v, ok := data[o.ConfigKey]

	if !ok {
		return value.Data{}, nil
	}

	s, ok := v.(string)
	typeOf := reflect.Indirect(reflect.ValueOf(o.Pointer)).Type()

	if typeOf == reflect.TypeOf(time.Second) && ok {
		d, err := time.ParseDuration(s)

		if err != nil {
			return value.Data{}, fmt.Errorf("%w: %s", ErrInvalidDuration, s)
		}

		v = d.Nanoseconds()
	}

	if !reflect.TypeOf(v).ConvertibleTo(typeOf) {
		return value.Data{}, fmt.Errorf("%w %T to %s for '%s'",
			ErrConversionFailure, v, typeOf.String(), o.ConfigKey)
	}

	return value.New(v), nil
}

func get(uri string) ([]byte, error) {
	ctx := context.Background()
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, uri, nil)

	if err != nil {
		return []byte{}, fmt.Errorf(
			"failed to request external config from %s: %w", uri, err)
	}

	r, err := http.DefaultClient.Do(req)

	if err != nil {
		return []byte{}, fmt.Errorf(
			"failed to get external config from %s: %w", uri, err)
	}

	defer func() { _ = r.Body.Close() }()

	b, err := ioutil.ReadAll(r.Body)

	if err != nil {
		return []byte{}, fmt.Errorf(
			"failed to read external config from %s: %w", uri, err)
	}

	return b, nil
}
