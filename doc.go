// Package goconfigure provides configuration options for an application,
// allowing configuration to come from a variety of sources.
package goconfigure
