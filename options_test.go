package goconfigure_test

import (
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
	"testing"
	"time"

	"bitbucket.org/idomdavis/goconfigure"
	"bitbucket.org/idomdavis/goconfigure/value"
)

func ExampleOptions_ParseUsing() {
	var options struct {
		value string
	}
	opts := &goconfigure.Options{Stub: "STUB"}
	opts.Add(opts.Option(&options.value, "", "value", "value to set"))

	if err := opts.ParseUsing([]string{"--value", "set"}); err != nil {
		fmt.Println(err)
	}

	fmt.Println(options)

	// Output:
	// {set}
}

func ExampleOptions_LoadUsing() {
	var options struct {
		config     string
		numeric    int
		text       string
		overridden string
	}

	opts := &goconfigure.Options{Stub: "STUB"}
	opts.Add(opts.Option(&options.text, "unset", "", "unset option"))
	opts.Add(opts.Option(&options.numeric, 0, "numeric", "unset option"))

	opts.Add(&goconfigure.Option{
		Pointer:   &options.overridden,
		ShortFlag: 'o',
	})

	opt := opts.Flag(&options.config, 'f', "config file")
	opts.Add(opt)
	opts.LoadUsing(opt)

	if err := opts.ParseUsing([]string{
		"-f", "testdata/config.json",
		"-o", "overridden",
	}); err != nil {
		fmt.Println(err)
	}

	fmt.Println(options)

	// Output:
	// {testdata/config.json 4 unset overridden}
}

func ExampleOptions_Add() {
	var options struct {
		boolean      bool
		integer      int
		long         int64
		unsigned     uint
		unsignedLong uint64
		float        float64
		text         string
		duration     time.Duration
	}

	opts := &goconfigure.Options{}

	opts.Add(opts.Flag(&options.boolean, 'b', "boolean"))

	opts.Add(opts.Flag(&options.integer, 'i', "integer"))
	opts.Add(opts.Flag(&options.long, 'l', "long"))
	opts.Add(opts.Flag(&options.unsigned, 'u', "unsigned"))
	opts.Add(opts.Flag(&options.unsignedLong, 'z', "unsignedLong"))
	opts.Add(opts.Flag(&options.float, 'f', "float"))
	opts.Add(opts.Flag(&options.text, 't', "text"))
	opts.Add(opts.Flag(&options.duration, 'd', "duration"))

	err := opts.ParseUsing([]string{
		"-b", "-i", "1", "-l", "2", "-u", "3", "-z", "4",
		"-f", "5.6", "-t", "words", "-d", "60s",
	})

	if err == nil {
		fmt.Println(options)
	} else {
		fmt.Println(err)
	}

	// Output:
	// {true 1 2 3 4 5.6 words 60000000000}
}

func TestOptions_Parse(t *testing.T) {
	t.Run("Parse uses OS arguments", func(t *testing.T) {
		var option string

		o := &goconfigure.Options{}
		o.Add(&goconfigure.Option{Pointer: &option, ShortFlag: 'o'})

		var args []string
		copy(args, os.Args)
		defer func() { copy(os.Args, args) }()
		os.Args = []string{"app", "-o", "set"}

		if err := o.Parse(); err != nil {
			t.Errorf("unexpected error parsing options: %v", err)
		}

		if option != "set" {
			t.Errorf("unexpect option value: %q", option)
		}
	})
}

func TestOptions_ParseUsing(t *testing.T) {
	t.Run("Invalid options will error", func(t *testing.T) {
		o := &goconfigure.Options{}
		o.Add(&goconfigure.Option{})

		err := o.ParseUsing([]string{})

		if !errors.Is(err, goconfigure.ErrNoPointer) {
			t.Errorf("unexpected error parsing options: %v", err)
		}
	})

	t.Run("Invalid flags will error", func(t *testing.T) {
		var p string

		o := &goconfigure.Options{}
		o.Add(&goconfigure.Option{
			Pointer:  &p,
			LongFlag: "flag",
			Default:  1,
		})

		err := o.ParseUsing([]string{})

		if !errors.Is(err, goconfigure.ErrInvalidDefault) {
			t.Errorf("unexpected error parsing options: %v", err)
		}
	})

	t.Run("Invalid environment variables will error", func(t *testing.T) {
		var p int

		o := &goconfigure.Options{}
		o.Add(&goconfigure.Option{
			Pointer: &p,
			EnvVar:  "TEST_VAR",
		})

		_ = os.Setenv("TEST_VAR", "test")

		err := o.ParseUsing([]string{})

		if !errors.Is(err, strconv.ErrSyntax) {
			t.Errorf("unexpected error parsing options: %v", err)
		}
	})

	t.Run("Invalid config options will error", func(t *testing.T) {
		var c, p string

		o := &goconfigure.Options{}
		o.Add(&goconfigure.Option{
			Pointer:   &p,
			ConfigKey: "count",
		})

		opt := o.Flag(&c, 'c', "config")
		o.Add(opt)
		o.LoadUsing(opt)

		err := o.ParseUsing([]string{"-c", "testdata/options.json"})

		if !errors.Is(err, goconfigure.ErrConversionFailure) {
			t.Errorf("unexpected error parsing options: %v", err)
		}
	})

	t.Run("Bad config file options will error", func(t *testing.T) {
		var p string

		o := &goconfigure.Options{}

		opt := &goconfigure.Option{Pointer: &p, Default: 1}
		o.Add(opt)
		o.LoadUsing(opt)

		err := o.ParseUsing([]string{})

		if !errors.Is(err, value.ErrAssignmentFailure) {
			t.Errorf("unexpected error parsing options: %v", err)
		}
	})

	t.Run("Non string config file URIs will error", func(t *testing.T) {
		var p int

		o := &goconfigure.Options{}

		opt := o.Flag(&p, 'c', "config")
		o.Add(opt)
		o.LoadUsing(opt)

		err := o.ParseUsing([]string{"-c", "1"})

		if !errors.Is(err, goconfigure.ErrFailedToReadURI) {
			t.Errorf("unexpected error parsing options: %v", err)
		}
	})

	t.Run("Errors parsing the flags are reported", func(t *testing.T) {
		var p int
		o := &goconfigure.Options{}
		o.Add(o.Flag(&p, 'v', "value"))

		err := o.ParseUsing([]string{"--notset"})

		if !errors.Is(err, goconfigure.ErrFlagError) {
			t.Errorf("unexpected error parsing options: %v", err)
		}
	})

	t.Run("Errors reading external config are reported", func(t *testing.T) {
		var p string

		o := &goconfigure.Options{}

		opt := o.Flag(&p, 'c', "config")
		o.Add(opt)
		o.LoadUsing(opt)

		err := o.ParseUsing([]string{"-c", "notfound"})

		if !errors.Is(err, goconfigure.ErrLoadingConfig) {
			t.Errorf("unexpected error parsing options: %v", err)
		}
	})

	t.Run("Errors parsing external config are reported", func(t *testing.T) {
		var c, p string

		o := &goconfigure.Options{}
		o.Add(&goconfigure.Option{Pointer: &c, ConfigKey: "count"})

		opt := o.Flag(&p, 'c', "config")
		o.Add(opt)
		o.LoadUsing(opt)

		err := o.ParseUsing([]string{"-c", "testdata/options.json"})

		if !errors.Is(err, goconfigure.ErrConversionFailure) {
			t.Errorf("unexpected error parsing options: %v", err)
		}
	})

	t.Run("Errors setting an option are reported", func(t *testing.T) {
		var p int

		o := &goconfigure.Options{}
		o.Add(&goconfigure.Option{Pointer: &p, Default: "default"})

		err := o.ParseUsing([]string{})

		if !errors.Is(err, value.ErrAssignmentFailure) {
			t.Errorf("unexpected error parsing options: %v", err)
		}
	})
}

func TestOptions_LoadUsing(t *testing.T) {
	t.Run("If no config file is given, don't error", func(t *testing.T) {
		var config string
		opts := &goconfigure.Options{}

		opt := opts.Flag(&config, 'f', "config file")
		opts.Add(opt)
		opts.LoadUsing(opt)

		if err := opts.ParseUsing([]string{}); err != nil {
			t.Errorf("Unexpected error with no config file: %v", err)
		}
	})
}

func TestOptions_Usage(t *testing.T) {
	t.Run("An empty options wont panic when running usage", func(t *testing.T) {
		opts := &goconfigure.Options{}
		opts.Usage(os.Stdout)
	})

	t.Run("A nil writer will not panic", func(t *testing.T) {
		opts := &goconfigure.Options{}
		opts.Usage(nil)
	})

	t.Run("Usage will panic on write failure", func(t *testing.T) {
		defer func() {
			if r := recover(); r == nil {
				t.Errorf("Usage did not panic")
			}
		}()

		opts := &goconfigure.Options{}
		opts.Usage(errWriter{})
	})
}

func TestOptions_UsageString(t *testing.T) {
	t.Run("Usage handles no options", func(t *testing.T) {
		opts := &goconfigure.Options{}
		s := opts.UsageString()

		if !strings.Contains(s, "No configuration options set") {
			t.Errorf("Unexpted usage output: %s", s)
		}
	})

	t.Run("Usage handles options", func(t *testing.T) {
		opts := &goconfigure.Options{}
		opts.Add(opts.Flag(nil, 'f', "Test option"))
		s := opts.UsageString()

		if !strings.Contains(s, "Test option") {
			t.Errorf("Unexpted usage output: %s", s)
		}
	})
}

func TestOptions_ToEnv(t *testing.T) {
	t.Run("An empty stub will not prefix _", func(t *testing.T) {
		opts := &goconfigure.Options{}
		env := opts.ToEnv("Test Var")

		if env != "TEST_VAR" {
			t.Errorf("Unexpected env var: %q", env)
		}
	})
}

type errWriter struct{}

func (errWriter) Write([]byte) (int, error) {
	//nolint:goerr113
	return 0, errors.New("write error")
}
