package goconfigure

import (
	"errors"
	"fmt"
	"reflect"
	"strings"

	"bitbucket.org/idomdavis/goconfigure/value"
)

// Option represents a configuration option that can be set either by flag,
// configuration file, environment variable, or a default value with the value
// to use being chosen in that order. Options must be one of bool, int, int64,
// uint, unit64, float64, string, or time.Duration.
type Option struct {
	// ShortFlag defines a short flag for setting the Option from the command
	// line. For example:
	//
	//     option.ShortFlag = 'f'
	//
	// allows the flag:
	//
	//     myApp -f value
	//
	// Setting this does nothing if Parse has already been called.
	ShortFlag rune

	// LongFlag defines a long flag for setting the Option from the command
	// line. For example:
	//
	//     option.LongFlag = "flag"
	//
	// allows the flag:
	//
	//     myApp --flag value
	//
	// Setting this does nothing if Parse has already been called.
	LongFlag string

	// EnvVar defines the name of an environment variable that can be used to
	// set this option. The string value of the environment variable must be
	// convertible to the type of the Option.
	//
	// Setting this does nothing if Parse has already been called.
	EnvVar string

	// ConfigKey defines the key this option can use to set itself from a JSON
	// configuration file. The value stored under this key must be convertible
	// to the Option Type.
	//
	// Setting this does nothing if Parse has already been called.
	ConfigKey string

	// Description is used when producing usage information. It should explain
	// what the Option is used for.
	Description string

	// Pointer to a variable of the type of this option (*bool, *int, *int64,
	// *uint, *uint64, *float64, *string, *time.Duration). Pointer will be set
	// when the option is Set.
	Pointer interface{}

	// Default defines a value that will be used by the option if no flags,
	// environment variables, or configuration file values are set or found. The
	// value must be the same type as the Option Pointer.
	Default interface{}
}

var (
	// ErrNoPointer is returned by Option.Check() if no pointer is set on the
	// Option.
	ErrNoPointer = errors.New("no pointer set on option")

	// ErrNotPointer is returned by Option.Check() if the type set on
	// Option.Pointer is not a pointer.
	ErrNotPointer = errors.New("incorrect type on option")
)

// Set the value for this option from one of the given values, or the default
// if none are set. The value chosen uses the precedence: short, long, env,
// external, Default.
func (o *Option) Set(short, long, env, external value.Data) error {
	var d value.Data

	switch {
	case short.Set:
		d = short
	case long.Set:
		d = long
	case env.Set:
		d = env
	case external.Set:
		d = external
	default:
		d = value.New(o.Default)
	}

	if err := d.AssignTo(o.Pointer); err != nil {
		return fmt.Errorf("failed to set option: %w", err)
	}

	return nil
}

// Check the Option is valid, returning an error if it is not.
func (o *Option) Check() error {
	if o.Pointer == nil {
		return fmt.Errorf("%w %q", ErrNoPointer, o.Description)
	}

	if reflect.TypeOf(o.Pointer).Kind() != reflect.Ptr {
		t := reflect.Indirect(reflect.ValueOf(o.Pointer)).Type().String()

		return fmt.Errorf("%w: %q should be *%s, not %[3]s", ErrNotPointer,
			o.Description, t)
	}

	return nil
}

// Usage information for this option.
//nolint: gocyclo,cyclop
func (o *Option) Usage() string {
	b := strings.Builder{}
	b.WriteString("\n  ")

	switch {
	case o.ShortFlag == 0 && o.LongFlag == "":
		b.WriteString("No CLI option")
	case o.ShortFlag != 0 && o.LongFlag != "":
		b.WriteString(fmt.Sprintf("-%c, --%s", o.ShortFlag, o.LongFlag))
	case o.ShortFlag != 0:
		b.WriteString(fmt.Sprintf("-%c", o.ShortFlag))
	case o.LongFlag != "":
		b.WriteString(fmt.Sprintf("--%s", o.LongFlag))
	}

	b.WriteString("\n        ")
	b.WriteString(strings.ReplaceAll(o.Description, "\n", "\n        "))

	var kind reflect.Kind

	if o.Default != nil {
		kind = value.Chase(o.Default).Kind()
	}

	quote := o.Default != nil && kind == reflect.String

	switch {
	case kind == reflect.Bool:
	case o.Default != nil && quote:
		b.WriteString(fmt.Sprintf(" (default %q)", o.Default))
	case o.Default != nil:
		b.WriteString(fmt.Sprintf(" (default %v)", o.Default))
	}

	if o.EnvVar != "" {
		b.WriteString("\n        Use $")
		b.WriteString(o.EnvVar)
		b.WriteString(" to set this using environment variables.")
	}

	if o.ConfigKey != "" {
		b.WriteString("\n        Use '")
		b.WriteString(o.ConfigKey)
		b.WriteString("' to set this in the config file.")
	}

	return b.String()
}

// Common values used by Sanitise.
const (
	SET   = "SET"
	UNSET = "UNSET"
)

// Sanitise a value, returning unset if v is nil or has a zero value, and set
// otherwise.
func Sanitise(v, set, unset interface{}) string {
	r := set

	if v == nil || reflect.ValueOf(v).IsZero() {
		r = unset
	}

	return fmt.Sprint(r)
}
