package goconfigure_test

import (
	"errors"
	"fmt"
	"testing"

	"bitbucket.org/idomdavis/goconfigure"
	"bitbucket.org/idomdavis/goconfigure/value"
)

func ExampleOption_Usage() {
	var (
		boolOpt   bool
		intOpt    int
		stringOpt string
	)
	o := &goconfigure.Option{
		ConfigKey:   "config-key",
		Description: "What the option does",
		Pointer:     &boolOpt,
	}

	fmt.Println(o.Usage())

	o.LongFlag = "flag"
	o.Pointer = &intOpt
	o.Default = 1

	fmt.Println(o.Usage())

	o.ShortFlag = 'f'
	o.EnvVar = "ENV"
	o.Pointer = &stringOpt
	o.Default = "unset"

	fmt.Println(o.Usage())

	// Output:
	// No CLI option
	//         What the option does
	//         Use 'config-key' to set this in the config file.
	//
	//   --flag
	//         What the option does (default 1)
	//         Use 'config-key' to set this in the config file.
	//
	//   -f, --flag
	//         What the option does (default "unset")
	//         Use $ENV to set this using environment variables.
	//         Use 'config-key' to set this in the config file.
}

func ExampleSanitise() {
	fmt.Println(goconfigure.Sanitise(nil, goconfigure.SET, goconfigure.UNSET))
	fmt.Println(goconfigure.Sanitise(1, goconfigure.SET, goconfigure.UNSET))

	// Output:
	// UNSET
	// SET
}

func TestOption_Set(t *testing.T) {
	var p string

	o := &goconfigure.Option{Pointer: &p, Default: "default"}

	short := value.New("short")
	long := value.New("long")
	env := value.New("env")
	external := value.New("external")
	unset := value.Data{}

	t.Run("short flag is set first", func(t *testing.T) {
		if err := o.Set(short, long, env, external); err != nil {
			t.Errorf("Unexpcted error setting options: %v", err)
		}

		if p != "short" {
			t.Errorf("Unexpected value for option: %v", o.Pointer)
		}
	})

	t.Run("long flag is set second", func(t *testing.T) {
		if err := o.Set(unset, long, env, external); err != nil {
			t.Errorf("Unexpcted error setting options: %v", err)
		}

		if p != "long" {
			t.Errorf("Unexpected value for option: %v", o.Pointer)
		}
	})

	t.Run("env var is set third", func(t *testing.T) {
		if err := o.Set(unset, unset, env, external); err != nil {
			t.Errorf("Unexpcted error setting options: %v", err)
		}

		if p != "env" {
			t.Errorf("Unexpected value for option: %v", o.Pointer)
		}
	})

	t.Run("external config is set forth", func(t *testing.T) {
		if err := o.Set(unset, unset, unset, external); err != nil {
			t.Errorf("Unexpcted error setting options: %v", err)
		}

		if p != "external" {
			t.Errorf("Unexpected value for option: %v", o.Pointer)
		}
	})

	t.Run("default is set last", func(t *testing.T) {
		if err := o.Set(unset, unset, unset, unset); err != nil {
			t.Errorf("Unexpcted error setting options: %v", err)
		}

		if p != "default" {
			t.Errorf("Unexpected value for option: %v", o.Pointer)
		}
	})

	t.Run("set will fail if the types are wrong", func(t *testing.T) {
		o.Default = 1

		err := o.Set(unset, unset, unset, unset)

		if !errors.Is(err, value.ErrAssignmentFailure) {
			t.Errorf("unexpected error parsing duration: %v", err)
		}
	})
}

func TestOption_Check(t *testing.T) {
	t.Run("An option with a valid Pointer is valid", func(t *testing.T) {
		var p string

		o := &goconfigure.Option{Pointer: &p}
		err := o.Check()

		if err != nil {
			t.Errorf("Unexpected error checking option: %v", err)
		}
	})

	t.Run("An option with a nil Pointer is invalid", func(t *testing.T) {
		o := &goconfigure.Option{}
		err := o.Check()

		if !errors.Is(err, goconfigure.ErrNoPointer) {
			t.Errorf("Unexpected error checking option: %v", err)
		}
	})

	t.Run("An option with non Pointer is invalid", func(t *testing.T) {
		o := &goconfigure.Option{Pointer: "not a pointer"}
		err := o.Check()

		if !errors.Is(err, goconfigure.ErrNotPointer) {
			t.Errorf("Unexpected error checking option: %v", err)
		}
	})
}
