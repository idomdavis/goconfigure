package goconfigure_test

import (
	"fmt"
	"os"

	"bitbucket.org/idomdavis/goconfigure"
)

func ExampleEnv_Register() {
	var (
		s string
		i int
	)

	const (
		stub = "GOCONFIGURE"
		name = "TEST_ENV_VAR"
		env  = stub + "_" + name
	)

	_ = os.Setenv(env, "set")
	defer func() { _ = os.Setenv(env, "") }()

	e := goconfigure.NewEnv()
	e.Stub = stub

	settings := goconfigure.NewSettings(stub)

	o := settings.Options.Option(&i, "", name, "")

	if err := e.Register(o); err != nil {
		fmt.Println("failed")
	}

	o.Pointer = &s

	if err := e.Register(o); err != nil {
		fmt.Println(err)
	}

	fmt.Println(e.Data[env].Pointer)

	// Output:
	// failed
	// set
}
